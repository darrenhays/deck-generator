import json
from flask import Blueprint, Response
from services.archidekt_service import get_cards
from utilities import get_request_data

archidekt_bp = Blueprint('archidekt', __name__)


@archidekt_bp.route("/decks/<deck_id>", methods=['POST'])
def get_cards_endpoint(deck_id, *args, **kwargs):
    limit = get_request_data('limit', 100)
    colors = get_request_data('colors', None)
    cards = get_cards(deck_id, limit, colors)
    return Response(json.dumps(cards), status=200, mimetype='text/json')
