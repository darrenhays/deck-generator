import json
import logging
from api.archidekt import archidekt_bp
from flask import Flask, Response


logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def register_blueprints(this_app):
    this_app.register_blueprint(archidekt_bp, url_prefix='/api/archidekt')


app = Flask(__name__)
register_blueprints(app)


@app.route('/', methods=['GET'])
def index():
    endpoints = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static' and "version" not in rule.arguments:
            endpoints[rule.rule] = app.view_functions[rule.endpoint].__doc__
    return Response(json.dumps(list(endpoints.keys())), status=200, mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True)
