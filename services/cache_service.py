import boto3


class CardPoolCache:
    def __init__(self):
        self.db = boto3.resource('dynamodb')
        self.table = self.db.Table('card_pools')

    def get(self, id):
        return self.table.get_item(Key={'id': id}).get('Item')

    def update(self, card_pool):
        self.table.put_item(Item=card_pool)
