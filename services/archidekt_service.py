import random
import requests
import settings
from services.cache_service import CardPoolCache
from zappa.asynchronous import task


def get_cards(deck_id, limit=100, colors=None):
    card_pool = update_card_pool_cache(deck_id)
    # update_card_pool_cache(deck_id)
    # card_pool = CardPoolCache().get(deck_id)
    all_cards = []

    # pull all cards out of the card pool
    for card in card_pool['cards']:
        for _ in range(card['quantity']):
            all_cards.append(card['name'])

    cards = []

    # randomly pull a card from all cards
    for _ in range(limit):
        try:
            n = random.randrange(len(all_cards))
            cards.append(all_cards.pop(n))
        except (ValueError, IndexError):
            pass

    return cards


# @task
def update_card_pool_cache(deck_id):
    deck_url = "{}/{}/".format(settings.ARCHIDEKT_DECK_ENDPOINT, str(deck_id))
    response = requests.get(deck_url)
    response_json = response.json()

    card_pool = {
        'id': deck_id,
        'source': 'archidekt',
        'url': deck_url,
        'cards': []
        # {
        #     'name': 'ABC',
        #     'quantity': '5',
        #     'colors': 'RGB'
        # },...
    }

    for n in range(len(response_json['cards'])):
        name = response_json['cards'][n]['card']['oracleCard']['name']
        quantity = response_json['cards'][n]['quantity']
        card_pool['cards'].append({'name': name, 'quantity': quantity})

    # CardPoolCache().update(card_pool)
    return card_pool
