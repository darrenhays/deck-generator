import os
from settings.base import *


if os.environ.get('environment') == 'production':
    from settings.production import *
else:
    from settings.development import *
